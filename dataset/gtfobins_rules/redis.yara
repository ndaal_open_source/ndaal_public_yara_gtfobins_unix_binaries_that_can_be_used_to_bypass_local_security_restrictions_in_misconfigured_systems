rule redis_file_write
{
    meta:
        id = "7WCoZ7aGxCrmVZ4uG2CKyj"
        fingerprint = "c78fae990b0bd2822d0516b039874fa31f334888651b54503f084ed61d5753b3"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using redis to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="IP=127.0.0.1 redis-cli -h $IP config set dir dir_to_write_to config set dbfilename file_to_write set x \"DATA\" save" nocase

condition:
    $a0
}

