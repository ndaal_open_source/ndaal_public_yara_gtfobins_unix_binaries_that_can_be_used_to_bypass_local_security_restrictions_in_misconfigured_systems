rule apt_shell
{
    meta:
        id = "5zASR9ZfYdw7RsB5EZtZih"
        fingerprint = "4e3062b97e68bbcb662ce19fc59cc446cbe4047cae9ef2f36297e67a2c633957"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using apt to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="apt changelog apt !/bin/sh" nocase

condition:
    $a0
}

rule apt_sudo
{
    meta:
        id = "3SGNM3Po3PDoJZ1u043G03"
        fingerprint = "fd034a55caa34c22510f7455165f65b179239de0e46a5a01d02af10b0d1b249f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using apt to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo apt changelog apt !/bin/sh" nocase
    $a1="TF=$(mktemp) echo 'Dpkg::Pre-Invoke {\"/bin/sh;false\"}' > $TF sudo apt install -c $TF sl" nocase

condition:
    ($a0 or $a1)
}

