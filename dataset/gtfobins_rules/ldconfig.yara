rule ldconfig_limited_suid
{
    meta:
        id = "6sFY7IlDpgkpSEsO6v731j"
        fingerprint = "cd48655dd516f3b7c79bd9d89459ed98a57b37b99de2df0bf21231625c76ac6b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ldconfig to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"$TF\" > \"$TF/conf\" # move malicious libraries in $TF ./ldconfig -f \"$TF/conf\"" nocase

condition:
    $a0
}

rule ldconfig_sudo
{
    meta:
        id = "2rWI97Npo4win964uaAJYk"
        fingerprint = "54ab2f8d79a24b5c4ec0aa15ddf37b90c0c7be92c8cb87cc37f8b90fa56ced83"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ldconfig to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"$TF\" > \"$TF/conf\" # move malicious libraries in $TF sudo ldconfig -f \"$TF/conf\"" nocase

condition:
    $a0
}

