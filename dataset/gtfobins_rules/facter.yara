rule facter_shell
{
    meta:
        id = "737Hmn6yqdJiYl1kCI8Qay"
        fingerprint = "e008fd5c983c9ed2ad2fb98fef7d2ab8050c924af3ee723bbebe4433c0a122b7"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using facter to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'exec(\"/bin/sh\")' > $TF/x.rb FACTERLIB=$TF facter" nocase

condition:
    $a0
}

rule facter_sudo
{
    meta:
        id = "60NtnLDmupQhqUl3cLV6wi"
        fingerprint = "d9f946ac7e834145f713fddaa632a94aef22348a14e5d5f19ef55255b10e79dd"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using facter to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'exec(\"/bin/sh\")' > $TF/x.rb sudo FACTERLIB=$TF facter" nocase

condition:
    $a0
}

