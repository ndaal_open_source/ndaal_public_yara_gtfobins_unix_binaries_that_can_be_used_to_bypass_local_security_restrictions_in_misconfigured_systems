rule vagrant_shell
{
    meta:
        id = "FETHXYZpjmeGKAD5euZsr"
        fingerprint = "5b1b4caa397665dbdae93a05749dcc51a3128063f3041b4e05be045a04e163bb"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vagrant to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="cd $(mktemp -d) echo 'exec \"/bin/sh\"' > Vagrantfile vagrant up" nocase

condition:
    $a0
}

rule vagrant_sudo
{
    meta:
        id = "2HdEMSXPtkaGFfNPK7B9B6"
        fingerprint = "5b1b4caa397665dbdae93a05749dcc51a3128063f3041b4e05be045a04e163bb"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vagrant to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="cd $(mktemp -d) echo 'exec \"/bin/sh\"' > Vagrantfile vagrant up" nocase

condition:
    $a0
}

rule vagrant_suid
{
    meta:
        id = "4LVNGWnz1EL1X4E4zp1Cgm"
        fingerprint = "4fd74200ab05ea94ce3ae237ff8423181f357ef9f9fcc0eac85a26ecf37c27b4"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vagrant to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="cd $(mktemp -d) echo 'exec \"/bin/sh -p\"' > Vagrantfile vagrant up" nocase

condition:
    $a0
}

