rule check_ssl_cert_command
{
    meta:
        id = "2kmNHYMom7XoRWBr9Ks9Tu"
        fingerprint = "70865769458b075a9077c31771d5cbc9bc1e58d6c74d2c6c8fb30dd6d5273f69"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_ssl_cert to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND=id OUTPUT=output_file TF=$(mktemp) echo \"$COMMAND | tee $OUTPUT\" > $TF chmod +x $TF check_ssl_cert --curl-bin $TF -H example.net cat $OUTPUT" nocase

condition:
    $a0
}

rule check_ssl_cert_sudo
{
    meta:
        id = "1AYIHF8qb3gl4O4zOdpcfe"
        fingerprint = "e6f9d0ef907600d87a25df9a92286fb0615ab8c340b659a056eff5423a882f90"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_ssl_cert to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND=id OUTPUT=output_file TF=$(mktemp) echo \"$COMMAND | tee $OUTPUT\" > $TF chmod +x $TF umask 022 check_ssl_cert --curl-bin $TF -H example.net cat $OUTPUT" nocase

condition:
    $a0
}

