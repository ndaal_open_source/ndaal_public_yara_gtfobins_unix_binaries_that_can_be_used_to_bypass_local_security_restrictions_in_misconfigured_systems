rule openvt_sudo
{
    meta:
        id = "5WtdiCxDfCkBZT9cSvp4Ms"
        fingerprint = "cf21378568f3b7f9881822e33d365f5d7ae18887c85a1013e422829426a10f9f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using openvt to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND=id TF=$(mktemp -u) sudo openvt -- sh -c \"$COMMAND >$TF 2>&1\" cat $TF" nocase

condition:
    $a0
}

