rule sqlmap_shell
{
    meta:
        id = "3TTr96mgXRG53qgNOnUQPz"
        fingerprint = "ab6e06fd088e15757f5811124cc2b71e85b8555fc266018497c191ad1b15477e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sqlmap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sqlmap -u 127.0.0.1 --eval=\"import os; os.system('/bin/sh')\"" nocase

condition:
    $a0
}

rule sqlmap_sudo
{
    meta:
        id = "5qSmjn2XBvdru6dXVX2F7S"
        fingerprint = "5779fb6f9c3586a2337cc9e5689f4c2d925e8cdc36f9fce63d963d76bf257375"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sqlmap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo sqlmap -u 127.0.0.1 --eval=\"import os; os.system('/bin/sh')\"" nocase

condition:
    $a0
}

