rule base32_file_read
{
    meta:
        id = "7NwIMwCRiKiT7QlV6thxtu"
        fingerprint = "32136b545a70b4e6873cb476ebdf677d30532b71f5bca374974d1ddb4bb91fed"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using base32 to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="base32 \"$LFILE\" | base32 --decode" nocase

condition:
    $a0
}

rule base32_sudo
{
    meta:
        id = "3AKL7HHhxUGpRI1ixe9kfI"
        fingerprint = "76d270aa624b8fb604841f980823dec963a15da5652d87f91aa62963ea6a9715"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using base32 to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo base32 \"$LFILE\" | base32 --decode" nocase

condition:
    $a0
}

rule base32_suid
{
    meta:
        id = "5OJSIkUYfp13Jnm7rcZjaH"
        fingerprint = "32136b545a70b4e6873cb476ebdf677d30532b71f5bca374974d1ddb4bb91fed"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using base32 to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="base32 \"$LFILE\" | base32 --decode" nocase

condition:
    $a0
}

