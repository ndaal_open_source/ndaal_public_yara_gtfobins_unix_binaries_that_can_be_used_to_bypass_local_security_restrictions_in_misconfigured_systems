rule dosbox_file_read
{
    meta:
        id = "2OQo6MSOmLBmaDkS4cuRKf"
        fingerprint = "cb107084b30fa86f836d7ab12458f48d2e70318352913f959a21d8b1d1efa2c8"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dosbox to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE='\\path\\to\\file_to_read' dosbox -c 'mount c /' -c \"type c:$LFILE\"" nocase
    $a1="LFILE='\\path\\to\\file_to_read' dosbox -c 'mount c /' -c \"copy c:$LFILE c:\\tmp\\output\" -c exit cat '/tmp/OUTPUT'" nocase

condition:
    ($a0 or $a1)
}

rule dosbox_file_write
{
    meta:
        id = "576IZqVGcDuL7m0Hda0aJ8"
        fingerprint = "3322d5dfb6b599c5c319492621d2605c4e658b9cdb11f3dcc521f6292ff6949e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dosbox to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE='\\path\\to\\file_to_write' dosbox -c 'mount c /' -c \"echo DATA >c:$LFILE\" -c exit" nocase

condition:
    $a0
}

rule dosbox_sudo
{
    meta:
        id = "3ODNHjAGadrpTWbd1ieziC"
        fingerprint = "29aa4aab2c95afce3c4214024dee4d8367092f5f05109ecf6c3f00c231a7e151"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dosbox to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE='\\path\\to\\file_to_write' sudo dosbox -c 'mount c /' -c \"echo DATA >c:$LFILE\" -c exit" nocase

condition:
    $a0
}

rule dosbox_suid
{
    meta:
        id = "5NdzeZg1oF5K8XAwrBSnFR"
        fingerprint = "c35a09724a4f25a7b0f79b5ab9566749580ba7681b339773f07e2f3aef9dc271"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dosbox to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE='\\path\\to\\file_to_write' ./dosbox -c 'mount c /' -c \"echo DATA >c:$LFILE\" -c exit" nocase

condition:
    $a0
}

