rule byebug_limited_suid
{
    meta:
        id = "356hTRdhQQDBlCVNpGZ4er"
        fingerprint = "4e0cc44eebf03284e35f10574135797a3e9a5030c2e993225e2fc62ee563c9b5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using byebug to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'system(\"/bin/sh\")' > $TF ./byebug $TF continue" nocase

condition:
    $a0
}

rule byebug_shell
{
    meta:
        id = "4TzK7eg7vdrYsrTjgGFIXl"
        fingerprint = "8bb8d62d7917ddfb3fb0a13caddd140efecc19f5b795c4073dd080b9d158b121"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using byebug to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'system(\"/bin/sh\")' > $TF byebug $TF continue" nocase

condition:
    $a0
}

rule byebug_sudo
{
    meta:
        id = "3cZ0tXGhEWgJNFcLsDiv7I"
        fingerprint = "b44d0b7846b0f50ba49b7f04f71218277e0905712a22f21aadc39c63b968cf60"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using byebug to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'system(\"/bin/sh\")' > $TF sudo byebug $TF continue" nocase

condition:
    $a0
}

