rule rpm_limited_suid
{
    meta:
        id = "4op9hsOFbjJdCkb2FnUalc"
        fingerprint = "631f20034696caca7694140185847271526fe71fefb8af46179ddab07ff16b57"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rpm to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./rpm --eval '%{lua:os.execute(\"/bin/sh\")}'" nocase

condition:
    $a0
}

rule rpm_shell
{
    meta:
        id = "3WTpr3vxwcTAC7oxGejoNu"
        fingerprint = "c0ed5e7bb7e2c819b4a93750f900e19b9a18a80ff462c99bf3081192a7769ea0"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rpm to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rpm --eval '%{lua:os.execute(\"/bin/sh\")}'" nocase
    $a1="rpm --pipe '/bin/sh 0<&1'" nocase

condition:
    ($a0 or $a1)
}

rule rpm_sudo
{
    meta:
        id = "61mBrxgEtj9TjREHcNHUr0"
        fingerprint = "f2f395ab8b2fce398a349554b8751993cbbd73bb0d6d4e82878d84ff666b4c6b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rpm to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo rpm --eval '%{lua:os.execute(\"/bin/sh\")}'" nocase
    $a1="sudo rpm -ivh x-1.0-1.noarch.rpm" nocase

condition:
    ($a0 or $a1)
}

