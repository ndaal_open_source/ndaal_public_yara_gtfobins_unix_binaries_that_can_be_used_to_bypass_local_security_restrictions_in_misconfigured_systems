rule dos2unix_file_write
{
    meta:
        id = "5iy17mEwwZYcRKYeZ0aAHP"
        fingerprint = "1a04411c3c15a2987694ae83c2edef27bc81bba43a2937d25841e2116c30e339"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dos2unix to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE1=file_to_read LFILE2=file_to_write dos2unix -f -n \"$LFILE1\" \"$LFILE2\"" nocase

condition:
    $a0
}

