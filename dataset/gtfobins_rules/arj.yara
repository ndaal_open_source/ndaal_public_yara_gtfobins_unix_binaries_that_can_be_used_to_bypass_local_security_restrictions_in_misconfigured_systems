rule arj_file_read
{
    meta:
        id = "3YCRf8iJIweNFvaqD5k7rg"
        fingerprint = "7bde2cba3c5150b220e323378cb349408493f04014c33b4fcda170b3e908793a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using arj to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -u) arj a \"$TF\" \"$LFILE\" arj p \"$TF\"" nocase

condition:
    $a0
}

rule arj_file_write
{
    meta:
        id = "5owsTybctX7BNQEenV7tDH"
        fingerprint = "0da9f9f1b9141d1f726bbaeea46154a892f36c85950e2989637fe7503a96121f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using arj to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) LDIR=where_to_write echo DATA >\"$TF/$LFILE\" arj a \"$TF/a\" \"$TF/$LFILE\" arj e \"$TF/a\" $LDIR" nocase

condition:
    $a0
}

rule arj_sudo
{
    meta:
        id = "6shgU8s17dRyvQS0lpYYBA"
        fingerprint = "5f65ca5ec06e6a33aa07a2ee53f8d8bca9a574341d860376d8d3f505b1fd6536"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using arj to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) LDIR=where_to_write echo DATA >\"$TF/$LFILE\" arj a \"$TF/a\" \"$TF/$LFILE\" sudo arj e \"$TF/a\" $LDIR" nocase

condition:
    $a0
}

rule arj_suid
{
    meta:
        id = "2T9XdpY2A4Gp5BnDmHDQSo"
        fingerprint = "92dcd6bc459c463ee0bd6c1b624659e0c7781668338a3bc28edbcabe6a528e3f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using arj to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) LDIR=where_to_write echo DATA >\"$TF/$LFILE\" arj a \"$TF/a\" \"$TF/$LFILE\" ./arj e \"$TF/a\" $LDIR" nocase

condition:
    $a0
}

