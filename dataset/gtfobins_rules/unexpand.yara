rule unexpand_file_read
{
    meta:
        id = "1R2kYtAd0xsK2uS3kA9GIr"
        fingerprint = "747ecdb42bf2190e14af770af2ef62fae88b49e4e565a82604164f05323e40b9"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using unexpand to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="unexpand -t99999999 \"$LFILE\"" nocase

condition:
    $a0
}

rule unexpand_sudo
{
    meta:
        id = "469qEL0DkFy1ebfrrwOXVQ"
        fingerprint = "c7951cd8c318afa2318b9c292c28140d487a2da86dc70f718f9d3b290bab84ce"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using unexpand to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo unexpand -t99999999 \"$LFILE\"" nocase

condition:
    $a0
}

rule unexpand_suid
{
    meta:
        id = "2ccRG7aTsdxuOk7wyBoNlt"
        fingerprint = "898bfe109f37eec0f5bebc724cd214dd45fc5ab626022ceeb96b4a705a2bb807"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using unexpand to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./unexpand -t99999999 \"$LFILE\"" nocase

condition:
    $a0
}

