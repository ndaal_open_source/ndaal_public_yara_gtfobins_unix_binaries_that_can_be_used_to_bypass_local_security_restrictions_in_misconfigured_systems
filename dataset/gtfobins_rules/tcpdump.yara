rule tcpdump_command
{
    meta:
        id = "7OrxLyRqvwMEmqU5hdtvXv"
        fingerprint = "ec11ef0132430c5c58a999ef8b29822da606180ab792747d6d57c9b974984910"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tcpdump to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='id' TF=$(mktemp) echo \"$COMMAND\" > $TF chmod +x $TF tcpdump -ln -i lo -w /dev/null -W 1 -G 1 -z $TF" nocase

condition:
    $a0
}

rule tcpdump_sudo
{
    meta:
        id = "4GIAZxmWabcieyKH7n6i7l"
        fingerprint = "f1d937b62fa52920df4c212a2b2bd1dffdfab3f1040ec3b0eaa6834f2e0b6944"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tcpdump to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='id' TF=$(mktemp) echo \"$COMMAND\" > $TF chmod +x $TF sudo tcpdump -ln -i lo -w /dev/null -W 1 -G 1 -z $TF -Z root" nocase

condition:
    $a0
}

