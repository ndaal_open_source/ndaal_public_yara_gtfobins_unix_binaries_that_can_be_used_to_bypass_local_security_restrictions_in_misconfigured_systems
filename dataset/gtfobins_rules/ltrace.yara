rule ltrace_file_read
{
    meta:
        id = "2S9umofw1pQhFmtbIuDTqu"
        fingerprint = "775f97f018fb3e1e43a29b0347df64c03452c06cdf856db3cd16113ed3dd265d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ltrace to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ltrace -F $LFILE /dev/null" nocase

condition:
    $a0
}

rule ltrace_file_write
{
    meta:
        id = "3kNQ9HAIP00Gn5zMgnEqdp"
        fingerprint = "4565916ae3923457c6bd6f34e0cfa33ca71131159e756cbcd8f48af0a75bcf96"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ltrace to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ltrace -s 999 -o $LFILE ltrace -F DATA" nocase

condition:
    $a0
}

rule ltrace_shell
{
    meta:
        id = "mMojDGJSMNQCa6V87VtI0"
        fingerprint = "a6195c6a6510b232b0647c894b8d39b85a7fa56407df883dc3b8fbbc0fd707f5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ltrace to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ltrace -b -L /bin/sh" nocase

condition:
    $a0
}

rule ltrace_sudo
{
    meta:
        id = "4cE8DHV3rF9OrfUoV7aKEp"
        fingerprint = "946ae8a64848b3edc931913ea1c7fa597287e2c01f56f1310ec81b29f551662a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ltrace to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo ltrace -b -L /bin/sh" nocase

condition:
    $a0
}

