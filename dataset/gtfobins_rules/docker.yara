rule docker_file_read
{
    meta:
        id = "6hOCORBHQYSXJiobnfe9Qf"
        fingerprint = "f1c067285979f41d223ef5337930c3be882202898f67ae1c63b840ac1c2255bd"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using docker to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="CONTAINER_ID=\"$(docker run -d alpine)\" # or existing TF=$(mktemp) docker cp file_to_read $CONTAINER_ID:$TF docker cp $CONTAINER_ID:$TF $TF cat $TF" nocase

condition:
    $a0
}

rule docker_file_write
{
    meta:
        id = "2mbl7PoLOK886t3EbHIzbb"
        fingerprint = "dcfd328c2adb8e60746cee89dc9ebb3c29fcd6d1c7eca4ba784d8352f91a6c8e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using docker to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="CONTAINER_ID=\"$(docker run -d alpine)\" # or existing TF=$(mktemp) echo \"DATA\" > $TF docker cp $TF $CONTAINER_ID:$TF docker cp $CONTAINER_ID:$TF file_to_write" nocase

condition:
    $a0
}

rule docker_shell
{
    meta:
        id = "4S4AvWg7hhCwjGIAIFhhG9"
        fingerprint = "82c3895b23ad4fe55db5b7aeb722d43e2baf7b00aa2fbc203c7b099d32514e5c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using docker to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="docker run -v /:/mnt --rm -it alpine chroot /mnt sh" nocase

condition:
    $a0
}

rule docker_sudo
{
    meta:
        id = "7efIWEAbIdUtLGq3s4E3gj"
        fingerprint = "f09f518236ad72ed16db6149bf85ef3b9bc4c033bc753923080faf00e8dc10a2"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using docker to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo docker run -v /:/mnt --rm -it alpine chroot /mnt sh" nocase

condition:
    $a0
}

rule docker_suid
{
    meta:
        id = "3uNTc4UnvKOO7gQKes13cy"
        fingerprint = "ba8efaaafd38098593ff59828e514eb398861aa5f15888e6cf654050a202cc84"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using docker to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./docker run -v /:/mnt --rm -it alpine chroot /mnt sh" nocase

condition:
    $a0
}

