rule crontab_command
{
    meta:
        id = "5QGJyegTtSJz8iOnkK1hpc"
        fingerprint = "1140d74fe5733014865f5a4847c0d937fef08f1f6aef9f682b1c8469cee0c986"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using crontab to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="crontab -e" nocase

condition:
    $a0
}

rule crontab_sudo
{
    meta:
        id = "3i65slX2ZZsHAYxVUSyFxD"
        fingerprint = "ea428ff397bcd98cff44288c1555156eaa8e34fac828083bbd41b9aac6e3b82c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using crontab to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo crontab -e" nocase

condition:
    $a0
}

