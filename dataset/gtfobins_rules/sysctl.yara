rule sysctl_command
{
    meta:
        id = "25y98p4RgnkeVge3cN6j1v"
        fingerprint = "013b87f2cf0ad6f030e087b5430f382acf01ea68998fb125c59d29018cc31e86"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sysctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='/bin/sh -c id>/tmp/id' sysctl \"kernel.core_pattern=|$COMMAND\" sleep 9999 & kill -QUIT $! cat /tmp/id" nocase

condition:
    $a0
}

rule sysctl_file_read
{
    meta:
        id = "3S2UjxISdeygg0FY6MbewQ"
        fingerprint = "d8ab8785e1f47fcfcf89c5a3cfa9e5bcfd52ca4811d5728bdfc46e0b22e3be52"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sysctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="/usr/sbin/sysctl -n \"/../../$LFILE\"" nocase

condition:
    $a0
}

rule sysctl_sudo
{
    meta:
        id = "1aK7kM5F81gnGUWhfCiX5R"
        fingerprint = "a3d7bed45225802354259cf4beb41bc00363e2688815b3384517c1e29fa331f1"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sysctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='/bin/sh -c id>/tmp/id' sudo sysctl \"kernel.core_pattern=|$COMMAND\" sleep 9999 & kill -QUIT $! cat /tmp/id" nocase

condition:
    $a0
}

rule sysctl_suid
{
    meta:
        id = "6nLByFSSpzUylw9hysKOFY"
        fingerprint = "aa3ea5055a734a9f1c10cd88220f6d947b7079b65a02344115972354bdb244dd"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using sysctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='/bin/sh -c id>/tmp/id' ./sysctl \"kernel.core_pattern=|$COMMAND\" sleep 9999 & kill -QUIT $! cat /tmp/id" nocase

condition:
    $a0
}

