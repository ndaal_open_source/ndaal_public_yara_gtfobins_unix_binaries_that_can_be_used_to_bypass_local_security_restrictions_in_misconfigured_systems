rule check_raid_file_read
{
    meta:
        id = "5CIXOQN4gOG8E4qwK9HPxy"
        fingerprint = "08073fdb569a26079d97594af82ca0c8582f72fe82e68bda7dccc32917fac15e"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_raid to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="check_raid --extra-opts=@$LFILE" nocase

condition:
    $a0
}

rule check_raid_sudo
{
    meta:
        id = "1UxMy9o73niF88xVLC9EHZ"
        fingerprint = "ac56513b5ba688e04234c336f49e089de99f6f8b68e286db374749d4e86608d6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_raid to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo check_raid --extra-opts=@$LFILE" nocase

condition:
    $a0
}

