rule split_command
{
    meta:
        id = "58uChn7B9H15T3DJ87oRZo"
        fingerprint = "a4438b54a23d9c60e9bfc9804728cd1def07ef76c5184645e58271cc3eb8872a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using split to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND=id TF=$(mktemp) split --filter=$COMMAND $TF" nocase
    $a1="COMMAND=id echo | split --filter=$COMMAND /dev/stdin" nocase

condition:
    ($a0 or $a1)
}

rule split_file_read
{
    meta:
        id = "2X6A3VKMB6pKTK9lLBQIHY"
        fingerprint = "92eb953dcddfe58f26cdef7e8fd02f3acb94d74359877c4ac17e2604f4b87018"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using split to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) split $LFILE $TF cat $TF*" nocase

condition:
    $a0
}

rule split_file_write
{
    meta:
        id = "1n8Zw8oK8DxEa41L2UrspR"
        fingerprint = "412b23308a5e89ebd747337e6890484a8bb60bbbe57b93df8178b13e5991bb9a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using split to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo DATA >$TF split -b999m $TF" nocase
    $a1="EXT=.xxx TF=$(mktemp) echo DATA >$TF split -b999m --additional-suffix $EXTENSION $TF" nocase

condition:
    ($a0 or $a1)
}

rule split_shell
{
    meta:
        id = "3GspqLLqXNr9GU6xrAQosM"
        fingerprint = "635a2fdcaba5bf6f5d00b0d377f96ce476b09bd25e7e0b03d6c8db6790832c90"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using split to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="split --filter=/bin/sh /dev/stdin" nocase

condition:
    $a0
}

rule split_sudo
{
    meta:
        id = "4TM8IsV9hBFkc7QYiUglU4"
        fingerprint = "f4d381711cdb351d15f9d845cafa30596681d3d2f1609ab14d58a5a1b1f15ee5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using split to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo split --filter=/bin/sh /dev/stdin" nocase

condition:
    $a0
}

