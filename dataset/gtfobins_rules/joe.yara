rule joe_limited_suid
{
    meta:
        id = "66CpLFpWYTsRNgQZjUoc1d"
        fingerprint = "9bb5f46075e9e74cc7ea8da88ee9bd7489aef8dbb7f46f5f2722669a7126d344"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using joe to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./joe ^K!/bin/sh" nocase

condition:
    $a0
}

rule joe_shell
{
    meta:
        id = "3qs9B8Fmf4eLlNwF8jl5nx"
        fingerprint = "c49364a2ea301305799367cd15685e9ef7db6bfa778c07f73476a8d6df422d0d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using joe to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="joe ^K!/bin/sh" nocase

condition:
    $a0
}

rule joe_sudo
{
    meta:
        id = "hpV9XqsT4NdC8E77v8s0F"
        fingerprint = "50762cf7a68a5cc8148c1c091ae24cd595d3185929bf82fadd84f15236acdcd5"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using joe to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo joe ^K!/bin/sh" nocase

condition:
    $a0
}

