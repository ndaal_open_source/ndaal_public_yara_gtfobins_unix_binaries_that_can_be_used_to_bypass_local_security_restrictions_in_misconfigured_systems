rule virsh_file_read
{
    meta:
        id = "3kNA9yzEiuKwu9yNMpU6UT"
        fingerprint = "06b0b8790243472f927752a2479e5fea849dbcc5bc6e27387e4797a29516d9a0"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using virsh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE_DIR=/root LFILE_NAME=file_to_read SPATH=file_to_save virsh -c qemu:///system pool-create-as x dir --target $LFILE_DIR virsh -c qemu:///system vol-download --pool x $LFILE_NAME $SPATH virsh -c qemu:///system pool-destroy x" nocase

condition:
    $a0
}

rule virsh_file_write
{
    meta:
        id = "532fx39jnbAoD3vKdCc1Of"
        fingerprint = "2f4293baac28c4d99d5d4f2e54eb91f9869126ee509575813b84c3aa12656851"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using virsh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE_DIR=/root LFILE_NAME=file_to_write echo 'data' > data_to_write TF=$(mktemp) cat > $TF <<EOF <volume type='file'> <name>y</name> <key>$LFILE_DIR/$LFILE_NAME</key> <source> </source> <capacity unit='bytes'>5</capacity> <allocation unit='bytes'>4096</allocation> <physical unit='bytes'>5</physical> <target> <path>$LFILE_DIR/$LFILE_NAME</path> <format type='raw'/> <permissions> <mode>0600</mode> <owner>0</owner> <group>0</group> </permissions> </target> </volume> EOF virsh -c qemu:///system pool-create-as x dir --target $LFILE_DIR virsh -c qemu:///system vol-create --pool x --file $TF virsh -c qemu:///system vol-upload --pool x $LFILE_DIR/$LFILE_NAME data_to_write virsh -c qemu:///system pool-destroy x" nocase

condition:
    $a0
}

rule virsh_sudo
{
    meta:
        id = "1CJK2xpPNBsuJak9bxYNA"
        fingerprint = "4460b392e9fc2b31ea88dcb79d6daa7b69619ca9e87d73e5b2d8a529ccb4cfd6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using virsh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="SCRIPT=script_to_run TF=$(mktemp) cat > $TF << EOF <domain type='kvm'> <name>x</name> <os> <type arch='x86_64'>hvm</type> </os> <memory unit='KiB'>1</memory> <devices> <interface type='ethernet'> <script path='$SCRIPT'/> </interface> </devices> </domain> EOF sudo virsh -c qemu:///system create $TF virsh -c qemu:///system destroy x" nocase

condition:
    $a0
}

