rule nohup_command
{
    meta:
        id = "1A8loVBoPtD0C9tymfIcs"
        fingerprint = "e29d1d2db523380375a79940a6766b010ef21b1f7b5d2192c4cf8138cafceb3f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nohup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='/usr/bin/id' nohup \"$COMMAND\" cat nohup.out" nocase

condition:
    $a0
}

rule nohup_shell
{
    meta:
        id = "6Gycu34hl9v2L1PJYYj1Yh"
        fingerprint = "e5d3371dc930ec140905f653ed0d6a641507d440cccf050dc699b515befc1588"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nohup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="nohup /bin/sh -c \"sh <$(tty) >$(tty) 2>$(tty)\"" nocase

condition:
    $a0
}

rule nohup_sudo
{
    meta:
        id = "zyUbZRM1b6X3LQsbAcm0m"
        fingerprint = "5b4383bbaa7cc3b5ec97282b6b9f13aaf6f61b8f764ca26aefd4f484c1788f5d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nohup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo nohup /bin/sh -c \"sh <$(tty) >$(tty) 2>$(tty)\"" nocase

condition:
    $a0
}

rule nohup_suid
{
    meta:
        id = "1mtJyaVvkHVHPokHPfVB1W"
        fingerprint = "ea5f0d470fb3bc0f524ee08a8929925f414547f2e0e3f0772fb8adcf959c0940"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using nohup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./nohup /bin/sh -p -c \"sh -p <$(tty) >$(tty) 2>$(tty)\"" nocase

condition:
    $a0
}

