rule dpkg_shell
{
    meta:
        id = "3M3vYuJDjjnRpO7LneJOtt"
        fingerprint = "0cd3851ed9a5ef873075fe99db1a8735fb51b1de636bb081bdd714fd7c431a46"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dpkg to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="dpkg -l !/bin/sh" nocase

condition:
    $a0
}

rule dpkg_sudo
{
    meta:
        id = "MyveTkKBpvXIWah9njQdl"
        fingerprint = "721d6a9b82f03bfcf66113e17fc6032e8e507ebe2a89a00bd5571f20254d7a1f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dpkg to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo dpkg -l !/bin/sh" nocase
    $a1="sudo dpkg -i x_1.0_all.deb" nocase

condition:
    ($a0 or $a1)
}

