rule cp_file_read
{
    meta:
        id = "6xjaweaWpBgbLmYAiR8Xvj"
        fingerprint = "a95c42408d85bb81ba309be027af8f2719374e58f3ac417c205f88d1eba17048"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="cp \"$LFILE\" /dev/stdout" nocase

condition:
    $a0
}

rule cp_file_write
{
    meta:
        id = "5Cfm4eQ2n9VfsxRzEU1uNG"
        fingerprint = "7d0af1a74b37c54965ecb8a1a6d5df8619ceb590538ef77d54d822793293c423"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"DATA\" | cp /dev/stdin \"$LFILE\"" nocase

condition:
    $a0
}

rule cp_sudo
{
    meta:
        id = "sJ6qHavytL3T4E3M8IBlM"
        fingerprint = "5ac1b58d05735480ceb31e961c67828ba275d3b3da2b08b9f41fc0179b4bdaa6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"DATA\" | sudo cp /dev/stdin \"$LFILE\"" nocase
    $a1="TF=$(mktemp) echo \"DATA\" > $TF sudo cp $TF $LFILE" nocase

condition:
    ($a0 or $a1)
}

rule cp_suid
{
    meta:
        id = "12z2ztz8UnfAnqi781R2h9"
        fingerprint = "587506a478d9755022fd3b243ac8249ee89c5e8784d8c96d2595025265e721c3"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cp to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"DATA\" | ./cp /dev/stdin \"$LFILE\"" nocase
    $a1="TF=$(mktemp) echo \"DATA\" > $TF ./cp $TF $LFILE" nocase

condition:
    ($a0 or $a1)
}

