rule gem_shell
{
    meta:
        id = "2IZhi6TzfZiN8UuQrHfe2Q"
        fingerprint = "bd6be4ab682cb6f56296ca065a5279c6ab09c49e20aca6f61d12db3b0ccd0eeb"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gem to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="gem open -e \"/bin/sh -c /bin/sh\" rdoc" nocase
    $a1="gem open rdoc :!/bin/sh" nocase

condition:
    ($a0 or $a1)
}

rule gem_sudo
{
    meta:
        id = "5hKIKw5QQJQTlW31obWG1Z"
        fingerprint = "1a95f3543e4a8382dcea83de8aa69793a11506f65fbb8f1e547484ada237b7b6"
        version = "1.0"
        date = "2023-07-13"
        modified = "2023-07-13"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gem to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo gem open -e \"/bin/sh -c /bin/sh\" rdoc" nocase

condition:
    $a0
}

